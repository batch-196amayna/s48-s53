import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView'
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'

import './App.css';
import {UserProvider} from './UserContext';
//we UserProviver from UserContext as this will store the user data


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    //email: localStorage.getItem('email')//no longer needed to store user details
  }); //this will get the email when user logged in

  const unsetUser = () => {
    localStorage.clear();
  }; //this will clear the local storage after logout

  useEffect(()=>{
    fetch('http://localhost:4000/users/getUserDetails', {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=> res.json())
    .then(data =>{
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data.id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null,
        });
      };
    });
  },[]);

    return(
      //for consecutive selfclosing tags, parent tag is needed
      <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
              <Route exact path="/courses" element={<Courses/>}/>
              <Route exact path="/register" element={<Register/>}/>
              <Route exact path="/login" element={<Login/>}/>
              <Route exact path="/logout" element={<Logout/>}/>
              <Route exact path="/courseview/:courseId" element={<CourseView/>}/>
              <Route exact path="*" element={<Error/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
      </>
    )
}

export default App;