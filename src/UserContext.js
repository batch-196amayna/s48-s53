import React from 'react';



//creates a context object
//context object -data type of an object that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext();

//provider component allows other components to consume/use the context object and supply the necessary
export const UserProvider = UserContext.Provider;

export default UserContext;





//Note: as the UserProvider has value we can use the UserContext to get the whole context: below to visualize
// UserContext = {
// 	user: '',
// 	setUser: function();
// 	unsetUser: function();
// };