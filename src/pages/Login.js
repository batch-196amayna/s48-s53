import {useState, useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
//UserProvider has already have content from App.js so we can now use the UserContext.js.

export default function Login(){

	//allows us to consume the user context object and it's properties to use for user validation
	//useContext(Provider);
	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function authenticate(e){
		e.preventDefault();

		//syntax: fetch('url', options)
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password:password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 196!"
				})

			} else {
				Swal.fire ({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})
			}
		});

		//set the email of the authenticated user in the local storage
		//syntax: localStorage.setItem("propertyName/Key", value);
		//localStorage.setItem("email", email); //this show the email in our localStorage

		//access user information through localStorage to the update thee user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
		//when state change components are rerendered and the AppNavbar component will be updated based on the credentials
		// setUser({
		// 	email: localStorage.getItem('email')
		// }); //no needed as it caused conflict with data id

		//clear input after submission
		setEmail('');
		setPassword('');

		//alert(`${email} has been verified! Welcome back!`);
	};

	const retrieveUserDetails = (token)=>{
		fetch('http://localhost:4000/users/getUserDetails', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	};

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password]);
	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Login Here</h1>
		<Form onSubmit={e => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value={password}
				onChange={e=> setPassword(e.target.value)}
				/>
			</Form.Group>

			<p>Not yet registered? <Link to="/register">Register Here</Link></p>

			{isActive?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Login</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Login</Button>
			}
		</Form>
		</>
	)
}