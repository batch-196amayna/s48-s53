import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);
	unsetUser(); //to clear from localhost

	//placing the "setUser" function inside of a useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated

	useEffect(()=>{
		setUser({id:null})
	}); //this will allow the logout page to render first before trigerring the useEffect which change the state of the user

	//localStorage.clear(); //no longer needed as we already have a provider

	return(
		<Navigate to="/login"/>
	)
}