import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProps}){
	//console.log(props);
	//console.log(typeof props);

	const {name, description, price, _id} = courseProps
	//object destructuring

	//react hooks - useState -> store its state
	//const [getter, setter] = useState(initialGetterValue)
	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(10);
	// console.log(useState(0));

	// 	function enroll(){
		
	// 	if (count !== 10 && seat !== 0){
	// 		setCount(count + 1);
	// 		console.log(`Enrollees: ${count}`);
	// 		setSeat(seat - 1);
	// 		console.log(`Seats: ${seat}`);
			
	// 	} else {
	// 		alert("No more seats available! check back later.");
	// 	}
	// };

	return(
	<Card className="cardCourse p-3">
		<Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Subtitle>Description</Card.Subtitle>
			<Card.Text>
				{description}
			</Card.Text>
			<Card.Subtitle>Price</Card.Subtitle>
			<Card.Text>{price}</Card.Text>
			<Link className="btn btn-primary" to={`/courseview/${_id}`}>View Details</Link>
			{/*<Card.Text>Enrollees: {count}</Card.Text>
			<Card.Text>Seats: {seat}</Card.Text>
			<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
		</Card.Body>
	</Card>
	
	)
};