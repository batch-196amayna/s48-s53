import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
//import AppNavbar from './components/AppNavbar'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  //only one entry point that will received data
  //StrictMode - highlights the information of error/problems
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



// const name = "Tony Starks";
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Levi',
//   lastName: 'Ackerman'
// };

// const formatName = (user)=>{
//   return user.firstName + " " + user.lastName;
// }

// const fullName = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullName);

/*

*/