import {useState, useEffect} from 'react';
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// const courses = coursesData.map(course => {
	// 	return(
	// 		<CourseCard key={course.id} courseProps={course}/>
	// 	)
	// }); //no longer needed as we already have working database

	const [courses, setCourses] = useState([]); //empty array
	 useEffect(()=>{
	 	fetch ('http://localhost:4000/courses')
	 	//no need for option as 'GET' option is already default

	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data);

	 		setCourses(data.map(course => {
	 			return(
	 				<CourseCard key={course._id} courseProps={course}/>
	 			)
	 		}))
	 	})
	 }, [])
	return(
			<>
				<h1>Available Courses:</h1>
				{courses}
			</>
		)
}

//process of passing through the props
//from the Courses Page -> CourseCard = PHP Laravel
//from CourseCard -. props === parameter
//courseProp from Courses Page === props parameter from CourseCard